package com.example.demomemorydb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;

@EnableAsync
@SpringBootApplication
public class DemoMemoryDbApplication {

    public static void main(String[] args) {
        SpringApplication.run(DemoMemoryDbApplication.class, args);
    }
}
