package com.example.demomemorydb.controller;

import com.example.demomemorydb.controller.request.CreateUserBusinessRequest;
import com.example.demomemorydb.controller.request.UpdateUserBusinessRequest;
import com.example.demomemorydb.entity.UserBusinessEntity;
import com.example.demomemorydb.exception.BaseException;
import com.example.demomemorydb.service.CreateBusinessProfileService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;
import java.util.UUID;

@RestController
@RequiredArgsConstructor
@RequestMapping("/user")
public class BusinessProfileController {
    //    private final ModelMapper modelMapper;
    @Autowired
    private CreateBusinessProfileService createBusinessProfileService;

    @PostMapping("/v1/business")
    public ResponseEntity<UUID> createUserBusiness(@RequestBody CreateUserBusinessRequest request) throws BaseException {
        UUID id = createBusinessProfileService.createUserBusiness(request);
        return ResponseEntity.ok(id);
    }

    @GetMapping("/v1/business/{id}")
    public ResponseEntity<Optional<UserBusinessEntity>> getByIdUserBusiness(@PathVariable("id") UUID userId) throws BaseException {
        Optional<UserBusinessEntity> response = createBusinessProfileService.getByIdUserBusiness(userId);
        return ResponseEntity.ok(response);
    }
    @PatchMapping("/v1/business/{id}")
    public ResponseEntity<UUID> updateUserBusiness(@PathVariable("id") UUID userId, @RequestBody UpdateUserBusinessRequest request) throws BaseException {
        UUID id = createBusinessProfileService.updateUserBusiness(userId, request);
        return ResponseEntity.ok(id);
    }
}
