package com.example.demomemorydb.controller;

import com.example.demomemorydb.controller.request.RegisterRequest;
import com.example.demomemorydb.controller.response.TestResponse;
import com.example.demomemorydb.exception.BaseException;
import com.example.demomemorydb.service.TestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
//@RequestMapping("/aoi") Before Path
public class TestApi {

    @Autowired
    private TestService testService;

//    TODO: Performants
//    private final TestService testService;
//
//    public TestApi(TestService testService) {
//        this.testService = testService;
//    }

    @GetMapping("/test")
    public TestResponse test() {
        TestResponse response = new TestResponse();
        response.setName("Aoi");
        response.setFood("Banana");
        return response;
    }

    @PostMapping("/register")
    public ResponseEntity<String> register(@RequestBody RegisterRequest request) throws BaseException {
        String response = testService.register(request);
        return ResponseEntity.ok(response);
    }
}
