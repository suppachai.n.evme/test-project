package com.example.demomemorydb.controller;

import com.example.demomemorydb.exception.BaseException;
import com.example.demomemorydb.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/user")
public class UserApi {
    @Autowired
    private UserService userService;

    @GetMapping("/{id}")
    public ResponseEntity<String> getUserById(@PathVariable("id") String id) throws BaseException {

        String response = userService.getUserById(id);
        return ResponseEntity.ok(response);
    }
}
