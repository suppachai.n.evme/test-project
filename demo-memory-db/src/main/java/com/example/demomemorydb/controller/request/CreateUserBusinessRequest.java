package com.example.demomemorydb.controller.request;

import com.sun.istack.NotNull;
import lombok.Data;

@Data
public class CreateUserBusinessRequest {
    @NotNull
    private String name;
}
