package com.example.demomemorydb.controller.request;

import com.sun.istack.NotNull;
import lombok.Data;

@Data
public class UpdateUserBusinessRequest {
    @NotNull
    private String name;
}
