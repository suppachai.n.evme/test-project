package com.example.demomemorydb.controller.response;

import lombok.Data;

@Data
public class TestResponse {
    private String name;
    private String food;
}
