package com.example.demomemorydb.repository;

import com.example.demomemorydb.entity.UserBusinessEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.UUID;

@Repository
public interface UserBusinessRepository extends JpaRepository<UserBusinessEntity, UUID>, JpaSpecificationExecutor<UserBusinessEntity> {
    Optional<UserBusinessEntity> findByName(String name);
}
