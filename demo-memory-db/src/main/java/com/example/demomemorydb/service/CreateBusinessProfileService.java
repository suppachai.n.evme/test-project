package com.example.demomemorydb.service;

import com.example.demomemorydb.controller.request.CreateUserBusinessRequest;
import com.example.demomemorydb.controller.request.UpdateUserBusinessRequest;
import com.example.demomemorydb.entity.UserBusinessEntity;
import com.example.demomemorydb.exception.BaseException;
import com.example.demomemorydb.exception.UserException;
import com.example.demomemorydb.repository.UserBusinessRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Optional;
import java.util.UUID;

@Slf4j
@Service
@RequiredArgsConstructor
public class CreateBusinessProfileService {
    private final UserBusinessRepository userBusinessRepository;

    @Transactional
    public UUID createUserBusiness(CreateUserBusinessRequest request) throws BaseException {
        log.info("Creating business profile");

//        checkDuplicateUserBusiness(request.getName());
//        log.info("Check name Duplicate success === {}", request.getName());

        UserBusinessEntity userBusinessEntity = new UserBusinessEntity();
        userBusinessEntity.setName(request.getName());

        UserBusinessEntity result = userBusinessRepository.save(userBusinessEntity);

        log.info("Business profile created");
        return result.getId();
    }

    public Optional<UserBusinessEntity> getByIdUserBusiness(UUID userId) throws BaseException {

        UserBusinessEntity userBusiness = userBusinessRepository.findById(userId)
                .orElseThrow(() -> UserException.notFound());

        log.info("Get Detail" + userBusiness);
        return Optional.of(userBusiness);
    }

    @Transactional
    public UUID updateUserBusiness(UUID userId, UpdateUserBusinessRequest request) throws BaseException {
        log.info("Update business profile");

        UserBusinessEntity userBusiness = userBusinessRepository.findById(userId)
                .orElseThrow(() -> UserException.notFound());
        log.info("Check business ID success === {}", userId);
        userBusiness.setName(request.getName());
        UserBusinessEntity result = userBusinessRepository.save(userBusiness);

        log.info("User Business Update");
        return result.getId();
    }

    private void checkDuplicateUserBusiness(String name) throws BaseException {
        final Optional<UserBusinessEntity> businessOptional = userBusinessRepository.findByName(name);
        if (businessOptional.isPresent()) {
            log.info("The name {} already exist.", name);
            throw UserException.notFound();
        }
    }

}
