package com.example.demomemorydb.service;

import com.example.demomemorydb.controller.request.RegisterRequest;
import com.example.demomemorydb.exception.BaseException;
import com.example.demomemorydb.exception.UserException;
import org.springframework.stereotype.Service;

import java.util.Objects;

@Service
public class TestService {
    public String register(RegisterRequest request) throws BaseException {
        if (request == null) {
            throw UserException.requestNull();
        }
        //validate email
        if (Objects.isNull(request.getEmail())) {
            throw UserException.emailNull();
        }
        return "Register" + request;
    }
}
