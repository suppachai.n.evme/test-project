package com.example.demomemorydb.service;

import com.example.demomemorydb.entity.UserEntity;
import com.example.demomemorydb.exception.BaseException;
import com.example.demomemorydb.exception.UserException;
import com.example.demomemorydb.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;

@Service
public class UserService {
    @Autowired
    private UserRepository userRepository;

    public String getUserById(String id) throws BaseException {
        // TODO: find in DB
        if (Objects.equals("1234", id)) {
            throw UserException.notFound();
        }
        List<UserEntity> result = userRepository.findAll();
        if (result == null) {
            throw UserException.notFound();
        }
        return id + " " + result.size() + " users";
    }
}
